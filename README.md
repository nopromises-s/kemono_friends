# Kemono Friends

## note
- https://4taba.net/ni/162

## Contents

### Songs

| File | Description |
| - | - |
| [`00_boku_no_friend`](https://nopromises-s.gitlab.io/kemono_friends/00_boku_no_friend.html) | 「ぼくのフレンド」<br>"My Friend" |
| [`00_kemono_parade`](https://nopromises-s.gitlab.io/kemono_friends/00_kemono_parade.html) | 「けものパレード～ジャパリパークメモリアル～」<br>"Kemono Parade \~Japari Park Memorial\~" |
| [`00_kimi_no_mama_de`](https://nopromises-s.gitlab.io/kemono_friends/00_kimi_no_mama_de.html) | 「きみのままで」<br>"Just the Way You Are" |
| [`00_oozora_dreamer`](https://nopromises-s.gitlab.io/kemono_friends/00_oozora_dreamer.html) | 「大空ドリーマー」<br>"Sky Dreamers" |
| [`00_ppp_no_doremi_no_uta`](https://nopromises-s.gitlab.io/kemono_friends/00_ppp_no_doremi_no_uta.html) | 「PPPのドレミのうた」<br>"PPP's Do-Re-Mi Song" |
| [`00_youkoso_japaripark_e`](https://nopromises-s.gitlab.io/kemono_friends/00_youkoso_japaripark_e.html) | 「ようこそジャパリパークへ」<br>"Welcome to Japari Park" |

### Subtitles

| File | Description |
| - | - |
| [`01_12-1_basuteki`](https://nopromises-s.gitlab.io/kemono_friends/01_12-1_basuteki.ass) | 12.1話「ばすてき」<br>Episode 12.1 "Bus-ish" |
| [`01_12-4_japarimangari`](https://nopromises-s.gitlab.io/kemono_friends/01_12-4_japarimangari.ass) | 12.4話「じゃぱりまんがり」<br>Episode 12.4 "Japari Bun Harvesting" |
| [`01_butai`](https://nopromises-s.gitlab.io/kemono_friends/01_butai.ass) | 舞台「けものフレンズ」（千秋楽）<br>Stage Play "Kemono Friends" (final performance)<hr>Many thanks to [加帕里图书馆](http://www.japari-cn.com/doku.php) for the original subtitle file |
| [`01_keibajou`](https://nopromises-s.gitlab.io/kemono_friends/01_keibajou.ass) | JRA×けものフレンズ すぺしゃる動画「けいばじょう」<br>JRA×*Kemono Friends* Special Video "Racecourse" |
| [`01_oshiete_juuishi-san!_mashita-onii-san_no_sarabureddo_kaisetsu`](https://nopromises-s.gitlab.io/kemono_friends/01_oshiete_juuishi-san!_mashita-onii-san_no_sarabureddo_kaisetsu.ass) | 教えて獣医師さん！ましたおにいさんのサラブレッド解説<br>Tell us, Veterinarian! Mashita-onii-san's Explanation of Thoroughbreds |
| [`01_youkoso_japari_park_e_cast_message`](https://nopromises-s.gitlab.io/kemono_friends/01_youkoso_japari_park_e_cast_message.ass) | - |
| [`2017-05-26_stream`](https://nopromises-s.gitlab.io/kemono_friends/2017-05-26_stream.ass) | Tatsuki live-stream |

### Guidebook

| File | Description |
| - | - |
| [`02_guidebook_01`](https://nopromises-s.gitlab.io/kemono_friends/02_guidebook_01.html) | Guidebook 1 |

### Text

| File | Description |
| - | - |
| [`2017-02-10_newtype`](https://nopromises-s.gitlab.io/kemono_friends/2017-02-10_newtype.html) | with Mine Yoshizaki |
| [`2017-02-21_blog`](https://nopromises-s.gitlab.io/kemono_friends/2017-02-21_blog.html) | by Mine Yoshizaki |
| [`2017-03-04_animatetimes`](https://nopromises-s.gitlab.io/kemono_friends/2017-03-04_animatetimes.html) | with Fukuhara Yoshitada and KADOKAWA's editor-in-chief |
| [`2017-03-27_excite`](https://nopromises-s.gitlab.io/kemono_friends/2017-03-27_excite.html) | with Fukuhara Yoshitada (1/2) |
| [`2017-05-26_stream`](https://nopromises-s.gitlab.io/kemono_friends/2017-05-26_stream.html) | Tatsuki live-stream |
| [`2017_jiken_twitter`](https://nopromises-s.gitlab.io/kemono_friends/2017_jiken_twitter.html) | |

## Notes

- Pink highlighting (`｛｝` in subtitles) means "please send help"
- Does [シリーズ]構成 translate to "story", or "[series] composition"
